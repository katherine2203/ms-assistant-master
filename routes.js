const express = require('express');
const router = express.Router();
const TextToSpeech = require('./services/TextToSpeech');

router.post('/create_session', function(req, res) {
  const assistant = req.assistant;

  assistant.createSession({
    assistantId: process.env.ASSISTANTID
  })
  .then(response => {
    const result = response.result;
    return res.json(result);
  })
  .catch(err => {
    return res.json({
      message: err
    });
  });
});

router.post('/message', function(req, res) {
  const assistant = req.assistant;
  assistant
  .message({
    assistantId: process.env.ASSISTANTID,
    sessionId: req.body.session_id,
    input: {
      'message_type': 'text',
      'text': req.body.text
    }
  })
  .then(response => {
    const result = response.result;
    return res.json(result);
  })
  .catch(err => {
    return res.json({
      message: err
    });
  });
});

module.exports = router;