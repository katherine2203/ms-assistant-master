"use strict";

const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

module.exports = function(req, res, next) {
  const assistant = new AssistantV2({
    version: process.env.VERSION,
    authenticator: new IamAuthenticator({
      apikey: process.env.APIKEY
    }),
    url: process.env.URL
  });
  req.assistant = assistant;
  next();
};